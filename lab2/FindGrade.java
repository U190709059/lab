public class FindGrade {
	
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		
		if (100 >= value && value >= 90){
			System.out.print("Your grade is A");
		}else if (90 > value && value >= 80) {
			System.out.print ("Your grade is B");
		}else if (80 > value && value >= 70) {
			System.out.print ("Your grade is C");
		}else if (70 > value && value >= 60) {
			System.out.print ("Your grade is D");
		}else if (60 > value && value >= 0) {
			System.out.print ("Your grade is F");
		}else {
			System.out.print ("It is not a valid score!");
		}
		}
}
	