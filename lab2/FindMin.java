public class FindMin {

	public static void main(String[] args){
		int value1=Integer.parseInt(args[0]);
		int value2=Integer.parseInt(args[1]);
		int value3=Integer.parseInt(args[2]);
		
		if (value1<value2 && value1<value3){
			System.out.println("The smallest number among the 3 aguments is "+ value1);
		}else if (value2<value1 && value2<value3){
			System.out.println("The smallest number among the 3 aguments is "+ value2);
		}else{
			System.out.println("The smallest number among the 3 aguments is "+ value3);
		}
	}
}