import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class FindPrimes {

    public static void main(String[] args){
        System.out.println(args[0]);
        int max = Integer.parseInt(args[0]);
		System.out.print("2");
        for (int number = 3; number <= max; number++){
            int divisor = 2;
            boolean isPrime = true;
            while (divisor < number && isPrime){
                if (number % divisor == 0){
                    isPrime = false;
                }
                divisor++;
            }


            if (isPrime)
                System.out.print(","+number);
        }
    }
}